cryptsetup -d /key/eap_hdd_key.bin --cipher=aes-xts-plain64 -s 256 --offset=0 --skip=111669149696 create ps4hdd /dev/sda27
mkdir /ps4hdd
mount -t ufs -o ufstype=ufs2 /dev/mapper/ps4hdd /ps4hdd
losetup /dev/loop0 /ps4hdd/home/linux.img
mount /dev/loop0 /newroot

echo "Booting linux (hopefully), please wait.." 
exec switch_root /newroot /newroot/sbin/init &
sleep 2 &&
exec switch_root /newroot /newroot/sbin/init &
sleep 2 &&
exec switch_root /newroot /newroot/sbin/init